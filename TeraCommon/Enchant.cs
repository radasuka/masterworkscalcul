﻿using System;

namespace TeraCommon
{
    public class Enchant : IEquatable<Enchant>
    {
        public enum CombatItemType
        {
            EQUIP_WEAPON,
            EQUIP_ARMOR_BODY,
            EQUIP_ARMOR_ARM,
            EQUIP_ARMOR_LEG,
            EQUIP_ACCESSORY
        }

        public Enchant(int rank, CombatItemType itemType, int enchantStep, double enchantProb, double common, double good)
        {
            Rank = rank;
            ItemType = itemType;
            EnchantStep = enchantStep;
            EnchantProb = enchantProb;
            Common = common;
            Good = good;
        }

        public int Rank { get; }
        public CombatItemType ItemType { get; }
        public int EnchantStep { get; }
        public double EnchantProb { get; }
        public double Common { get; }
        public double Good { get; }

        public override int GetHashCode()
        {
            return (Rank + (int)ItemType + EnchantProb + EnchantStep).GetHashCode();
        }

        public bool Equals(Enchant y)
        {
            if ((object)y == null)
                return false;
            return (Rank == y.Rank) && (ItemType == y.ItemType) && (EnchantStep == y.EnchantStep) && (EnchantProb == y.EnchantStep);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Enchant e = obj as Enchant;
            if ((System.Object)e == null)
                return false;
            return this.Equals(e);
        }
    }
}
