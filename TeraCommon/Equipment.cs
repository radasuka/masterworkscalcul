﻿namespace TeraCommon
{
    public class Equipment
    {
        public enum CombatItemType
        {
            EQUIP_WEAPON,
            EQUIP_ARMOR_BODY,
            EQUIP_ARMOR_ARM,
            EQUIP_ARMOR_LEG,
            EQUIP_ACCESSORY
        }

        public Equipment(int id, string name, int rank, CombatItemType itemType, int balance, int defense, int impact, int maxAtk, int minAtk, double masterpieceRate, bool awakenable)
        {
            Id = id;
            Name = name;
            Rank = rank;
            ItemType = itemType;
            Balance = balance;
            Defense = defense;
            Impact = impact;
            MaxAtk = maxAtk;
            MinAtk = minAtk;
            MasterpieceRate = masterpieceRate;
            Awakenable = awakenable;
        }

        public int Id { get; }
        public string Name { get; }
        public int Rank { get; }
        public CombatItemType ItemType { get; }
        public int Balance { get; }
        public int Defense { get; }
        public int Impact { get; }
        public int MaxAtk { get; }
        public int MinAtk { get; }
        public double MasterpieceRate { get; }
        public bool Awakenable { get; }
    }
}
