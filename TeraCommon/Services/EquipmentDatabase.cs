﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TeraCommon
{
    public class EquipmentDatabase
    {
        private readonly Dictionary<int, Equipment> _equipments = new Dictionary<int, Equipment>();

        public EquipmentDatabase(string folder, string language)
        {
            var reader = new StreamReader(File.OpenRead(Path.Combine(folder, $"Equipment\\Equipment-{language}.tsv")));
            while(!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line == null) continue;
                var values = line.Split('\t');
                var id = int.Parse(values[0]);
                var name = values[1];
                var rank = int.Parse(values[2]);
                var itemType = (Equipment.CombatItemType)Enum.Parse(typeof(Equipment.CombatItemType), values[3]);
                var balance = int.Parse(values[4]);
                var defense = int.Parse(values[5]);
                var impact = int.Parse(values[6]);
                var maxAtk = int.Parse(values[7]);
                var minAtk = int.Parse(values[8]);
                var masterpieceRate = double.Parse(values[9]);
                var awakenable = bool.Parse(values[10]);

                _equipments[id] = new Equipment(id, name, rank, itemType, balance, defense, impact, maxAtk, minAtk, masterpieceRate, awakenable);
            }
        }

        public void Add(Equipment eq)
        {
            _equipments[eq.Id] = eq;
        }

        public Equipment Get(int id)
        {
            return !_equipments.ContainsKey(id) ? null : _equipments[id];
        }

        public List<Equipment> GetList()
        {
            return _equipments.Values.ToList();
        }
    }
}
