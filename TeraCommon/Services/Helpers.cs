﻿using System;
using System.Collections.Concurrent;

namespace TeraCommon
{
    public static class Helpers
    {
        public static Func<T, TResult> Memoize<T, TResult>(Func<T, TResult> func)
        {
            var lookup = new ConcurrentDictionary<T, TResult>();
            return x => lookup.GetOrAdd(x, func);
        }
    }
}
