﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TeraCommon
{
    public class EnchantDatabase
    {
        private readonly Dictionary<int, Enchant> _enchantData = new Dictionary<int, Enchant>();

        public EnchantDatabase(string folder, string language)
        {
            var index = 0;
            var reader = new StreamReader(File.OpenRead(Path.Combine(folder, $"Equipment\\enchantData-{language}.tsv")));
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line == null) continue;
                var values = line.Split('\t');
                var rank = int.Parse(values[0]);
                var itemType = (Enchant.CombatItemType)Enum.Parse(typeof(Enchant.CombatItemType), values[1]);
                var enchantStep = int.Parse(values[2]);
                var enchantProb = double.Parse(values[3]);
                var common = double.Parse(values[4]);
                var good = double.Parse(values[5]);

                _enchantData[index] = new Enchant(rank, itemType, enchantStep, enchantProb, common, good);
                index++;
            }
        }

        public Enchant Get(int id)
        {
            return !_enchantData.ContainsKey(id) ? null : _enchantData[id];
        }

        public Enchant Get(int rank, Enchant.CombatItemType itemType, int step)
        {
            return _enchantData.Where(x =>
                                      x.Value.Rank == rank &&
                                      x.Value.ItemType == itemType &&
                                      x.Value.EnchantStep == step).FirstOrDefault().Value;
        }

        public List<Enchant> GetList()
        {
            return _enchantData.Values.ToList();
        }
    }
}
