﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeraCommon;

namespace Data
{
    public class BasicTeraData
    {
        private static BasicTeraData _instance;
        private readonly Func<string, TeraData> _dataForRegion;
        private static readonly ILog _log = LogManager.GetLogger("MasterworksCalcul");
        private static int _errorCount = 5;

        private BasicTeraData() : this(FindResourceDirectory())
        {

        }
        private BasicTeraData(string resourceDirectory)
        {
            ResourceDirectory = resourceDirectory;
            Directory.CreateDirectory(Path.Combine(resourceDirectory, "config"));
            XmlConfigurator.Configure(new Uri(Path.Combine(ResourceDirectory, "log4net.xml")));
            WindowData = new WindowData(this);
            _dataForRegion = Helpers.Memoize<string, TeraData>(region => new TeraData(region));
        }

        public static BasicTeraData Instance => _instance ?? (_instance = new BasicTeraData());
        public string ResourceDirectory { get; }
        public EquipmentDatabase EquipmentDatabase { get; set; }
        public EnchantDatabase EnchantDatabase { get; set; }
        public WindowData WindowData { get; }

        public TeraData DataForRegion(string region)
        {
            return _dataForRegion(region);
        }

        private static string FindResourceDirectory()
        {
            var directory = Path.Combine(typeof(BasicTeraData).Assembly.Location);
            while(directory != null)
            {
                var resourceDirectory = Path.Combine(directory, @"resources");
                if (Directory.Exists(resourceDirectory))
                    return resourceDirectory;
                directory = Path.GetDirectoryName(directory);
            }
            throw new InvalidOperationException("リソースディレクトリが見つかりませんでした。");
        }

        public static void LogError(string error, bool debug = false)
        {
            if (debug && _errorCount-- <= 0) return;
            Task.Run(() =>
            {
                try
                {
                    error = $"#### (version=1.0):\r\n" + (debug ? "#### Debug: " : "") + error;
                    _log.Error(error);
                }
                catch
                {

                }
            });
        }
    }
}
