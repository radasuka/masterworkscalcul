﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;

namespace Data
{
    public class WindowData
    {
        private readonly FileStream _fileStream;
        private readonly XDocument _xml;

        public Point Location { get; set; }
        public double Scale { get; set; }
        public string Language { get; private set; }
        public bool AutoUpdate { get; private set; }

        private void DefaultValue()
        {
            Location = new Point(0, 0);
            Language = "Auto";
            Scale = 1;
            AutoUpdate = true;
        }

        public WindowData(BasicTeraData basicData)
        {
            DefaultValue();
            // XMLロード
            var windowFile = Path.Combine(basicData.ResourceDirectory, "config/window.xml");

            try
            {
                var attrs = File.GetAttributes(windowFile);
                File.SetAttributes(windowFile, attrs & ~FileAttributes.ReadOnly);
            }
            catch
            {

            }

            try
            {
                _fileStream = new FileStream(windowFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                _xml = new XDocument(_fileStream);
            }
            catch(Exception ex) when (ex is XmlException || ex is InvalidOperationException)
            {
                Save();
                _fileStream = new FileStream(windowFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                return;
            }
            catch
            {
                return;
            }

            Parse("scale", "Scale");
            Parse("autoupdate", "AutoUpdate");

            ParseLocation();
            ParseLanguage();
        }

        private void ParseLocation()
        {
            double x, y;
            var root = _xml.Root;

            var location = root?.Element("location");
            if (location == null) return;
            var xElement = location.Element("x");
            var yElement = location.Element("y");
            if (xElement == null || yElement == null) return;

            var xParsed = double.TryParse(xElement.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out x);
            var yParsed = double.TryParse(yElement.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out y);
            if(xParsed && yParsed)
            {
                Location = new Point(x, y);
            }
        }

        private void ParseLanguage()
        {
            var root = _xml.Root;
            var languageElement = root?.Element("language");
            if (languageElement == null) return;
            Language = languageElement.Value;
            if (
                !Array.Exists(new[] { "Auto", "EU-EN", "EU-FR", "EU-GER", "NA", "RU", "JP", "TW", "KR" },
                s => s.Equals(Language))) Language = "Auto";
        }

        private void Parse(string xmlName, string settingName)
        {
            var root = _xml.Root;
            var xml = root?.Element(xmlName);
            if (xml == null) return;
            var setting = this.GetType().GetProperty(settingName);
            if(setting.PropertyType == typeof(int))
            {
                int value;
                var parseSuccess = int.TryParse(xml.Value, out value);
                if(parseSuccess)
                {
                    setting.SetValue(this, value, null);
                }
            }
            if (setting.PropertyType == typeof(double))
            {
                double value;
                var parseSuccess = double.TryParse(xml.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out value);
                if (parseSuccess)
                {
                    setting.SetValue(this, value, null);
                }
            }
            if (setting.PropertyType == typeof(float))
            {
                float value;
                var parseSuccess = float.TryParse(xml.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out value);
                if (parseSuccess)
                {
                    setting.SetValue(this, value, null);
                }
            }
            if (setting.PropertyType == typeof(bool))
            {
                bool value;
                var parseSuccess = bool.TryParse(xml.Value, out value);
                if (parseSuccess)
                {
                    setting.SetValue(this, value, null);
                }
            }
            if (setting.PropertyType == typeof(string))
            {
                setting.SetValue(this, xml.Value, null);
            }
        }

        public void Save()
        {
            if (_fileStream == null)
                return;

            var xml = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), new XElement("window"));
            xml.Root.Add(new XElement("location"));
            xml.Root.Element("location").Add(new XElement("x", Location.X.ToString(CultureInfo.InvariantCulture)));
            xml.Root.Element("location").Add(new XElement("y", Location.Y.ToString(CultureInfo.InvariantCulture)));
            xml.Root.Add(new XElement("language", Language));
            xml.Root.Add(new XElement("autoupdate", AutoUpdate));
            xml.Root.Add(new XElement("scale", Scale.ToString(CultureInfo.InvariantCulture)));

            _fileStream.SetLength(0);
            using (var sw = new StreamWriter(_fileStream, new UTF8Encoding(true)))
            {
                sw.Write(xml.Declaration + Environment.NewLine + xml);
            }
            _fileStream.Close();
        }
    }
}
