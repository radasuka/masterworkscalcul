﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeraCommon;

namespace Data
{
    public class TeraData
    {
        internal TeraData(string region)
        {
            var language = GetLanguage(region);

            BasicTeraData.Instance.EquipmentDatabase = 
                new EquipmentDatabase(Path.Combine(BasicTeraData.Instance.ResourceDirectory, "data/"), language);
            BasicTeraData.Instance.EnchantDatabase =
                new EnchantDatabase(Path.Combine(BasicTeraData.Instance.ResourceDirectory, "data/"), language);
        }

        public string GetLanguage(string region)
        {
            if(BasicTeraData.Instance.WindowData.Language == "Auto")
            {
                return region == "EU" ? "EU-EN" : region;
            }
            return BasicTeraData.Instance.WindowData.Language;
        }
    }
}
