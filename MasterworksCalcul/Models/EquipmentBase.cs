﻿using Livet;

namespace MasterworksCalcul.Models
{
    public abstract class EquipmentBase : NotificationObject
    {
        protected readonly double[] MasterpiecesPer = { 0.01, 0.02, 0.03 };

        ///<summary>
        /// 強化+1の上昇値
        ///</summary>
        protected double StrengtheningRisevalue { set; get; }
        /// <summary>
        /// 現在の強化上昇値（名品補正なし）
        /// </summary>
        protected double PresentStrengtheningRisevalue { set; get; }
        /// <summary>
        /// 強化段階
        /// </summary>
        protected int EnchantStep { set; get; }
        /// <summary>
        /// 基礎値
        /// </summary>
        protected int BasicValue { set; get; }
        /// <summary>
        /// 名品補正値
        /// </summary>
        protected int[] NotedArticleCorrectionValue = new int[3];
        /// <summary>
        /// 強化上昇値(名品補正あり)
        /// </summary>
        protected int[] PresentStrengtheningRisevalues = new int[3];

        public abstract string Calcul(int basicValue, int enchantStep);
    }
}
