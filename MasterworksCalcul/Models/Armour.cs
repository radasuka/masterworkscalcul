﻿namespace MasterworksCalcul.Models
{
    public class Armour : EquipmentBase
    {
        private static Armour _instance;
        public static Armour Instance => _instance ?? (_instance = new Armour());

        private Armour() : base() { }

        public override string Calcul(int basicValue, int enchantStep)
        {
            StrengtheningRisevalue = 0;
            PresentStrengtheningRisevalue = 0;

            EnchantStep = enchantStep;
            BasicValue = basicValue;

            // 強化上昇値(補正なし)
            switch (EnchantStep)
            {
                case 13:    // 覚醒13
                    var per = 1.0 + ((4.5 * 12 + 6) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;

                case 14:    // 覚醒15
                    per = 1.0 + ((4.5 * 12 + 6 + 9) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;

                case 15:    // 覚醒15
                    per = 1.0 + ((4.5 * 12 + 6 + 9 + 12) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;

                default:    // 名品0～12
                    per = 1.0 + ((4.5 * EnchantStep) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;
            }
            var str = $"基本性能: {BasicValue}\n";

            for (int index = 0; index < PresentStrengtheningRisevalues.Length; ++index)
            {
                // 強化上昇値(補正あり)
                PresentStrengtheningRisevalues[index] = (int)((int)PresentStrengtheningRisevalue + BasicValue * MasterpiecesPer[index]);
                // 名品ボーナス補正値
                NotedArticleCorrectionValue[index] = PresentStrengtheningRisevalues[index] - (int)PresentStrengtheningRisevalue;

                str += $"名品補正: {(MasterpiecesPer[index] * 100)}%\n";
                str += $"強化上昇値: {PresentStrengtheningRisevalues[index] - BasicValue}\n";
                str += $"補正値: {NotedArticleCorrectionValue[index]}\n";
                str += $"合計値: {PresentStrengtheningRisevalues[index]}\n\n";
            }
            return str;
        }
    }
}
