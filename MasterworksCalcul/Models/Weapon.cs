﻿using System;

namespace MasterworksCalcul.Models
{
    public class Weapon : EquipmentBase
    {
        private static Weapon _instance;
        public static Weapon Instance => _instance ?? (_instance = new Weapon()); 
        private Weapon(): base() { }

        /// <summary>
        /// 補正値計算
        /// </summary>
        public override string Calcul(int basicValue, int enchantStep)
        {
            StrengtheningRisevalue = 0;
            PresentStrengtheningRisevalue = 0;

            EnchantStep = enchantStep;
            BasicValue = basicValue;

            // 強化上昇値(補正なし)
            switch (EnchantStep)
            {
                case 14:    // 覚醒14
                    var per = 1.0 + ((3 * 13 + 4) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;

                case 15:    // 覚醒15
                    per = 1.0 + ((3 * 13 + 4 + 5) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;

                default:
                    per = 1.0 + ((3 * EnchantStep) / 100);
                    PresentStrengtheningRisevalue = BasicValue * per;
                    break;
            }
            var str = string.Empty;
            str = $"基本性能: {BasicValue}\n";

            for (int index = 0; index < PresentStrengtheningRisevalues.Length; ++index)
            {
                // 強化上昇値(補正あり)
                PresentStrengtheningRisevalues[index] = (int)((int)PresentStrengtheningRisevalue + BasicValue * MasterpiecesPer[index]);
                // 名品ボーナス補正値
                NotedArticleCorrectionValue[index] = PresentStrengtheningRisevalues[index] - (int)PresentStrengtheningRisevalue;

                str += $"名品補正: {(MasterpiecesPer[index] * 100)}%\n";
                str += $"強化上昇値: {PresentStrengtheningRisevalues[index] - BasicValue}\n";
                str += $"補正値: {NotedArticleCorrectionValue[index]}\n";
                str += $"合計値: {PresentStrengtheningRisevalues[index]}\n\n";
            }
            return str;
        }
    }
}
