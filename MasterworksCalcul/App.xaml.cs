﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

using Livet;
using System.Threading;
using Data;
using log4net;
using AutoUpdate;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace MasterworksCalcul
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private Mutex _unique;


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        private async void Application_Startup(object sender, StartupEventArgs e)
        {
            bool aIsNewInstance;
            bool notUpdating;

            var updating = new Mutex(true, "MasterworksCalculUpdating", out notUpdating);
            _unique = new Mutex(true, "MasterworksCalcul", out aIsNewInstance);

            DispatcherHelper.UIDispatcher = Dispatcher;
            //AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            if (aIsNewInstance)
            {
                DeleteTmp();
                if(BasicTeraData.Instance.WindowData.Language != "Auto")
                {
                    // TODO:LP.Culture
                }
                if(!BasicTeraData.Instance.WindowData.AutoUpdate)
                {
                    return;
                }
                var shutdown = false;
                try
                {
                    shutdown = await CheckUpdate();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(
                        "アップデートサーバーへの接続に失敗しました。");
                    var log = LogManager.GetLogger(typeof(Program));
                    log.Error("##### UPDATE EXCEPTION (version=" + UpdateManager.Version + "): #####\r\n" + ex.Message +
                              "\r\n" +
                              ex.StackTrace + "\r\n" + ex.Source + "\r\n" + ex + "\r\n" + ex.Data + "\r\n" +
                              ex.InnerException +
                              "\r\n" + ex.TargetSite);
                }
                if (!shutdown) return;
                Current.Shutdown();
                Process.GetCurrentProcess().Kill();
                Environment.Exit(0);
            }

            if(!notUpdating)
            {
                SetForeground();
            }
            bool isWaitingUpdateEnd;
            var waitUpdateEnd = new Mutex(true, "MasterworksCalculWaitUpdateEnd", out isWaitingUpdateEnd);
            if(!isWaitingUpdateEnd)
            {
                SetForeground();
            }
            updating.WaitOne();
            DeleteTmp();
            updating.Close();
            waitUpdateEnd.Close();
        }

        private void DeleteTmp()
        {
            try
            {
                if (Directory.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\tmp\"))
                {
                    Directory.Delete(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\tmp\", true);
                }
            }
            catch
            {

            }
        }

        private static void SetForeground()
        {
            var current = Process.GetCurrentProcess();
            foreach(
                var process in
                    Process.GetProcessesByName(current.ProcessName).Where(process => process.Id != current.Id))
            {
                SetForegroundWindow(process.MainWindowHandle);
                break;
            }
            Current.Shutdown();
            Process.GetCurrentProcess().Kill();
            Environment.Exit(0);
        }

        private static async Task<bool> CheckUpdate()
        {
            var isUpToDate = await UpdateManager.IsUpToDate().ConfigureAwait(false);
            if(isUpToDate)
            {
                return false;
            }

            if (MessageBox.Show("アプリケーションを更新しますか？", "利用可能な更新", MessageBoxButton.YesNo,
                MessageBoxImage.Question) != MessageBoxResult.Yes) return false;
            return await UpdateManager.Update();
        }

        //集約エラーハンドラ
        //private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        //{
        //    //TODO:ロギング処理など
        //    MessageBox.Show(
        //        "不明なエラーが発生しました。アプリケーションを終了します。",
        //        "エラー",
        //        MessageBoxButton.OK,
        //        MessageBoxImage.Error);
        //
        //    Environment.Exit(1);
        //}
    }
}
