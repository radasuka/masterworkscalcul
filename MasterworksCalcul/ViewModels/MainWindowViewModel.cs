﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using MasterworksCalcul.Models;
using System.Windows.Data;
using TeraCommon;
using Data;
using System.Threading;
using System.Windows;
using Application = System.Windows.Forms.Application;
using MessageBox = System.Windows.MessageBox;

namespace MasterworksCalcul.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        /* コマンド、プロパティの定義にはそれぞれ 
         * 
         *  lvcom   : ViewModelCommand
         *  lvcomn  : ViewModelCommand(CanExecute無)
         *  llcom   : ListenerCommand(パラメータ有のコマンド)
         *  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
         *  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)
         *  
         * を使用してください。
         * 
         * Modelが十分にリッチであるならコマンドにこだわる必要はありません。
         * View側のコードビハインドを使用しないMVVMパターンの実装を行う場合でも、ViewModelにメソッドを定義し、
         * LivetCallMethodActionなどから直接メソッドを呼び出してください。
         * 
         * ViewModelのコマンドを呼び出せるLivetのすべてのビヘイビア・トリガー・アクションは
         * 同様に直接ViewModelのメソッドを呼び出し可能です。
         */

        /* ViewModelからViewを操作したい場合は、View側のコードビハインド無で処理を行いたい場合は
         * Messengerプロパティからメッセージ(各種InteractionMessage)を発信する事を検討してください。
         */

        /* Modelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedEventListenerや
         * CollectionChangedEventListenerを使うと便利です。各種ListenerはViewModelに定義されている
         * CompositeDisposableプロパティ(LivetCompositeDisposable型)に格納しておく事でイベント解放を容易に行えます。
         * 
         * ReactiveExtensionsなどを併用する場合は、ReactiveExtensionsのCompositeDisposableを
         * ViewModelのCompositeDisposableプロパティに格納しておくのを推奨します。
         * 
         * LivetのWindowテンプレートではViewのウィンドウが閉じる際にDataContextDisposeActionが動作するようになっており、
         * ViewModelのDisposeが呼ばれCompositeDisposableプロパティに格納されたすべてのIDisposable型のインスタンスが解放されます。
         * 
         * ViewModelを使いまわしたい時などは、ViewからDataContextDisposeActionを取り除くか、発動のタイミングをずらす事で対応可能です。
         */

        /* UIDispatcherを操作する場合は、DispatcherHelperのメソッドを操作してください。
         * UIDispatcher自体はApp.xaml.csでインスタンスを確保してあります。
         * 
         * LivetのViewModelではプロパティ変更通知(RaisePropertyChanged)やDispatcherCollectionを使ったコレクション変更通知は
         * 自動的にUIDispatcher上での通知に変換されます。変更通知に際してUIDispatcherを操作する必要はありません。
         */
        private List<Equipment> _equipmentList;
        private List<Enchant> _enchantList;
        private List<int> _rankList;
        public TeraData TeraData { get; private set; }

        public void Initialize()
        {
            Application.ThreadException += GlobalThreadExceptionHandler;
            System.Windows.Application.Current.Resources["Scale"] = BasicTeraData.Instance.WindowData.Scale;

            TeraData = BasicTeraData.Instance.DataForRegion("JP");
            _equipmentList = BasicTeraData.Instance.EquipmentDatabase.GetList();
            _enchantList = BasicTeraData.Instance.EnchantDatabase.GetList();
            _rankList = _equipmentList.Select(x => x.Rank).Distinct().OrderBy(x => x).ToList();
            RankListView = new ListCollectionView(_rankList);
            RankListView.CurrentChanged += _rankListView_CurrentChanged;
            EquipmentListView = new ListCollectionView(_equipmentList);
            EquipmentListView.CurrentChanged += EquipmentListView_CurrentChanged;
            EnchantListView = new ListCollectionView(new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            CanClose = false;
            WindowTitle = "MasterworksCalcul" + AutoUpdate.UpdateManager.Version.ToString();

            EquipmentListView.Filter = b =>
            {
                var eq = b as Equipment;
                if (eq == null) return false;
                var rank = (int)RankListView.CurrentItem;
                return eq.Rank == rank;
            };
            EnchantListView.Filter = b =>
            {
                var eqItem = EquipmentListView.CurrentItem as Equipment;
                if (eqItem == null) return false;
                if (eqItem.Awakenable)
                    return (int)b <= 15;
                return (int)b <= 12;
            };
        }

        private string _windowTitle;
        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                if (_windowTitle == value)
                    return;
                _windowTitle = value;
                RaisePropertyChanged();
            }
        }
        private ListCollectionView _rankListView;
        public ListCollectionView RankListView
        {
            get { return _rankListView; }
            set
            {
                if (_rankListView == value)
                    return;
                _rankListView = value;
                RaisePropertyChanged();
            }
        }

        private ListCollectionView _equipmentListView;
        public ListCollectionView EquipmentListView
        {
            get { return _equipmentListView; }
            set
            {
                if (_equipmentListView == value)
                    return;
                _equipmentListView = value;
                RaisePropertyChanged();
            }
        }

        private ListCollectionView _enchantListView;
        public ListCollectionView EnchantListView
        {
            get { return _enchantListView; }
            set
            {
                if (_enchantListView == value)
                    return;
                _enchantListView = value;
                RaisePropertyChanged();
            }

        }

        private string _resultText;
        public string RisultText
        {
            get { return _resultText; }
            set
            {
                if (_resultText == value)
                    return;
                _resultText = value;
                RaisePropertyChanged();
            }
        }

        private bool _canClose;
        public bool CanClose
        {
            get { return _canClose; }
            set
            {
                if (_canClose == value)
                    return;
                _canClose = value;
                RaisePropertyChanged();
            }
        }

        private void _rankListView_CurrentChanged(object sender, EventArgs e)
        {
            EquipmentListView.Filter = b =>
            {
                var eq = b as Equipment;
                if (eq == null) return false;
                var rank = (int)RankListView.CurrentItem;
                return eq.Rank == rank;
            };
        }

        private void EquipmentListView_CurrentChanged(object sender, EventArgs e)
        {
            var lv = sender as ICollectionView;
            if (lv.CurrentPosition < 0)
            {
                System.Diagnostics.Trace.WriteLine("選択なし");
                return;
            }
            var eq = lv.CurrentItem as Equipment;
            System.Diagnostics.Trace.WriteLine($"CurrentChanged:位置={lv.CurrentPosition},名前={eq.Name}");

            EnchantListView.Filter = b =>
            {
                var eqItem = EquipmentListView.CurrentItem as Equipment;
                if (eqItem == null) return false;
                if (eqItem.Awakenable)
                    return (int)b <= 15;
                return (int)b <= 12;
            };
        }

        private static void GlobalThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            var ex = e.Exception;
            BasicTeraData.LogError("##### FORM EXCEPTION #####\r\n" + ex.Message + "\r\n" +
                     ex.StackTrace + "\r\n" + ex.Source + "\r\n" + ex + "\r\n" + ex.Data + "\r\n" + ex.InnerException +
                     "\r\n" + ex.TargetSite);
            MessageBox.Show("");
        }

        public void Calcul()
        {
            var currentItem = EquipmentListView.CurrentItem as Equipment;
            if (currentItem == null)
            {
                RisultText = "装備データを取得できませんでした。";
                BasicTeraData.LogError(RisultText);
                return;
            }
            var step = (int)EnchantListView.CurrentItem;
            var str = string.Empty;
            switch (currentItem.ItemType)
            {
                case Equipment.CombatItemType.EQUIP_WEAPON:
                    str = Weapon.Instance.Calcul(currentItem.MaxAtk, step);
                    break;
                case Equipment.CombatItemType.EQUIP_ARMOR_BODY:
                case Equipment.CombatItemType.EQUIP_ARMOR_ARM:
                case Equipment.CombatItemType.EQUIP_ARMOR_LEG:

                    str = Armour.Instance.Calcul(currentItem.Defense, step);
                    break;
                default:
                    str = "強化可能装備ではありません。";
                    break;
            }
            str += NextEnchantProb(currentItem, step);
            RisultText = str;
        }

        private string NextEnchantProb(Equipment equipment, int step)
        {
            var str = new StringBuilder();
            var enchant = BasicTeraData.Instance.EnchantDatabase.Get(equipment.Rank, (Enchant.CombatItemType)equipment.ItemType, step);
            if (enchant == null) return str.ToString();
            if (enchant.EnchantStep == 12 && !equipment.Awakenable) return str.ToString();

            str.AppendLine($"EnchantProb: {enchant.EnchantProb}");
            str.AppendLine($"EnchantProb(%): {enchant.EnchantProb * 100}%");
            str.AppendLine($"<<EnchantSuccessRating>>");
            str.AppendLine($"一般: {enchant.Common}");
            str.AppendLine($"神秘: {enchant.Good}");

            return str.ToString();
        }

        public void CloseCanceledCallback()
        {
            var message = new ConfirmationMessage("終了してよろしいですか？",
                                                  "確認",
                                                  MessageBoxImage.Information,
                                                  MessageBoxButton.OKCancel,
                                                  "Confirm");

            // View側がメッセージを処理し終えるまでブロックされる
            Messenger.Raise(message);

            // 何もしなければ、WindowのCloseはキャンセルされたまま
            if (message.Response != true) return;

            BasicTeraData.Instance.WindowData.Save();

            // 改めてWindowを閉じる
            CanClose = true;

            DispatcherHelper.UIDispatcher.BeginInvoke((Action)(() => Messenger.Raise(new WindowActionMessage(WindowAction.Close, "WindowAction"))));
        }
    }
}
