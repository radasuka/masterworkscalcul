@echo off
msbuild MasterworksCalcul.sln /p:Configuration=Release /p:Platform="Any CPU"
set output=.\MasterworksCalculBin
set source=.
set variant=Release
rmdir /Q /S "%output%"
md "%output%
md "%output%\resources"
md "%output%\resources\config"

xcopy "%source%\MasterworksCalcul\bin\%variant%" "%output%\" /E
xcopy "%source%\resources" "%output%\resources\" /E
del "%output%\*.xml"
del "%output%\error.log"
del "%output%\*.vshost*"
del "%output%\*.pdb"
del "%output%\resources\config\*.xml"
