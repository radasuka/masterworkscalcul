﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace AutoUpdate
{
    public class Program
    {
        private static void Main(string[] args)
        {
            if(args.Length == 0)
            {
                Console.WriteLine("更新システムは変更されあなたのバージョンと互換性がありません。Webサイトから直接新しいバージョンをダウンロードしてください。");
                Console.ReadLine();
                return;
            }

            bool aIsNewInstance, isUpdating;
            var _unique = new Mutex(true, "MasterworksCalcul", out aIsNewInstance);
            if(!aIsNewInstance)
            {
                while(!_unique.WaitOne(1000))
                {
                    Console.WriteLine("Sleep");
                }
            }
            Thread.Sleep(1000);
            var uniqueUpdating = new Mutex(true, "MasterworksCalculUpdating", out isUpdating);

            UpdateManager.DestroyRelease();
            var source = Directory.GetDirectories(UpdateManager.ExecutableDirectory + @"\..\release\")[0];
            UpdateManager.Copy(source, UpdateManager.ExecutableDirectory + @"\..\..\");
            Console.WriteLine("新バージョンインストール");
            //Process.Start("explorer.exe", "");// TODO:パッチノート
            Process.Start(UpdateManager.ExecutableDirectory + @"\..\..\MasterworksCalcul.exe");
            Environment.Exit(0);
        }
    }
}
