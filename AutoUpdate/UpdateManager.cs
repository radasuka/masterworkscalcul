﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace AutoUpdate
{
    public class UpdateManager
    {
        public static readonly string Version = "1.06";

        public static string ExecutableDirectory => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public static string ResourcesDirectory
        {
            get
            {
                var directory = Path.GetDirectoryName(typeof(UpdateManager).Assembly.Location);
                while (directory != null)
                {
                    var resourceDirectory = Path.Combine(directory, @"resources");
                    if (Directory.Exists(resourceDirectory))
                        return resourceDirectory;
                    directory = Path.GetDirectoryName(directory);
                }
                throw new InvalidOperationException("リソースディレクトリが見つかりませんでした。");
            }
        }

        public static async Task<bool> Update()
        {
            var isUpToDate = await IsUpToDate().ConfigureAwait(false);
            if (isUpToDate) return false;
            Download();
            return true;
        }

        public static async Task<bool> IsUpToDate()
        {
            var latestVersion = await LatestVersion().ConfigureAwait(false);
            Console.WriteLine("現在のバージョン = " + Version);
            Console.WriteLine("最新のバージョン = " + latestVersion);
            return latestVersion == Version;
        }

        private static void Decompress(string latestVersion)
        {
            ZipFile.ExtractToDirectory(ExecutableDirectory + @"\tmp\" + latestVersion, ExecutableDirectory + @"\tmp\");
            ZipFile.ExtractToDirectory(ExecutableDirectory + @"\tmp\" + latestVersion, ExecutableDirectory + @"\tmp\release\");
        }

        private static void Download()
        {
            DestroyDownloadDirectory();
            Directory.CreateDirectory(ExecutableDirectory + @"\tmp\release\");


            var latestVersion = "MasterworksCalcul" + LatestVersion().Result;
            Console.WriteLine("最新版のダウンロード開始");
            using (var client = new WebClient())
            {
                client.DownloadFile(
                    "https://bitbucket.org/radasuka/masterworkscalcul/downloads/" + latestVersion +
                    ".zip", ExecutableDirectory + @"\tmp\" + latestVersion + ".zip");
            }
            Console.WriteLine("最新版のダウンロード完了");
            Console.WriteLine("解凍開始");
            Decompress(latestVersion + ".zip");
            Console.WriteLine("解凍完了");
            Process.Start(ExecutableDirectory + @"\tmp\" + latestVersion + @"\Autoupdate.exe", "pass");
            Console.WriteLine("アップデート開始");
        }

        private static void DestroyDownloadDirectory()
        {
            if (!Directory.Exists(ExecutableDirectory + @"\tmp\")) return;
            Directory.Delete(ExecutableDirectory + @"\tmp\", true);
        }

        public static void Copy(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);
            foreach (var file in Directory.GetFiles(sourceDir))
            {
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)), true);
            }

            foreach (var directory in Directory.GetDirectories(sourceDir))
            {
                if (directory == "config")
                {
                    Directory.CreateDirectory(targetDir);
                    continue;
                }
                Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
            }
        }

        public static void DestroyRelease()
        {
            Array.ForEach(Directory.GetFiles(ExecutableDirectory + @"\..\..\"), File.Delete);
            Array.ForEach(Directory.GetFiles(ExecutableDirectory + @"\..\..\resources\"), File.Delete);
            foreach (var s in Directory.GetDirectories(ExecutableDirectory + @"\..\..\").Where(t => !(t.EndsWith("resources") || t.EndsWith("tmp"))))
            {
                if (Directory.Exists(s))
                {
                    Directory.Delete(s, true);
                }
            }
            if (!Directory.Exists(ExecutableDirectory + @"\..\..\resources\")) return;
            foreach(var s in Directory.GetDirectories(ExecutableDirectory + @"\..\..\resources\").Where(t => !t.EndsWith("config")))
            {
                if (Directory.Exists(s))
                {
                    Directory.Delete(s, true);
                }
            }
            Console.WriteLine("Resourcesフォルダ削除");
        }

        private static async Task<string> LatestVersion()
        {
            var version =
                await
                GetResponseText("https://bitbucket.org/radasuka/masterworkscalcul/downloads/version.txt")
                .ConfigureAwait(false);
            version = Regex.Replace(version, @"\r\n?|\n", "");

            return version;
        }

        private static async Task<string> GetResponseText(string address)
        {
            return await GetResponseText(address, 3);
        }

        private static async Task<string> GetResponseText(string address, int numbertry)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetByteArrayAsync(new Uri(address));
                    return Encoding.UTF8.GetString(response, 0, response.Length);
                }
            }
            catch (Exception)
            {
                if (numbertry > 0)
                {
                    return await GetResponseText(address, numbertry - 1);
                }
                throw;
            }
        }
    }
}
